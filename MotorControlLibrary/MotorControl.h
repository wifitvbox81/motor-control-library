 /*

  MIT License

  Copyright (c) 2020 Owais Shaikh

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/

class MotorControl {
  public:
    int m_p, s_p_1, s_p_2;
    MotorControl (int signal_pin_1, int signal_pin_2) {  // H-bridge with DC Motor
        s_p_1 = signal_pin_1;
        s_p_2 = signal_pin_2;
    }

    MotorControl (int motor_pin) {  // Simple DC Motor and servo signal pin
        m_p = motor_pin;
    }

    void controlMotor (String direction, int speed, long on_duration, long off_duration) { // Looping speed and direction control
      if (direction=="counter") {
        pinMode(s_p_1, OUTPUT);
        pinMode(s_p_2, OUTPUT);
        analogWrite(s_p_1, speed);
        delay(on_duration);
        analogWrite(s_p_1, 0);
        delay(off_duration);
      } else if (direction=="clock") {
        pinMode(s_p_1, OUTPUT);
        pinMode(s_p_2, OUTPUT);
        analogWrite(s_p_2, speed);
        delay(on_duration);
        analogWrite(s_p_2, 0);
        delay(off_duration);
      }
    }

    void controlMotor (double ramp_speed, String direction) {  // ramp control with direction
      if (direction=="counter") {
        ramp_speed=255-ramp_speed;
        for (int value=0; value<=255; value++) {
          pinMode(s_p_1, OUTPUT);
          pinMode(s_p_2, OUTPUT);
          analogWrite(s_p_1, value);
          analogWrite(s_p_2, 0);
          delay(ramp_speed);
        }
      } else if (direction=="clock") {
        ramp_speed=255-ramp_speed;
        for (int value=0; value<=255; value++) {
          pinMode(s_p_1, OUTPUT);
          pinMode(s_p_2, OUTPUT);
          analogWrite(s_p_1, 0);
          analogWrite(s_p_2, value);
          delay(ramp_speed);
        }
      }
    }

    void controlMotor (double ramp_speed) {  // ramp control
      ramp_speed=255-ramp_speed;
      for (int value=0; value<=255; value++) {
        analogWrite(m_p, value);
        delay(ramp_speed);
      }
    }

    void controlMotor (int speed, String direction) {  // Speed and direction control
      if (direction=="counter") {
        pinMode(s_p_1, OUTPUT);
        pinMode(s_p_2, OUTPUT);
        analogWrite(s_p_1, speed);
        analogWrite(s_p_2, 0);
      } else if (direction=="clock") {
        pinMode(s_p_1, OUTPUT);
        pinMode(s_p_2, OUTPUT);
        analogWrite(s_p_1, 0);
        analogWrite(s_p_2, speed);
      }
    }

    void controlMotor (int speed) {  // Simple speed control
      pinMode(m_p, OUTPUT);
      analogWrite(m_p, speed);
    }

    void controlMotor (int speed, int on_duration, int off_duration) {  // Looping speed control
      analogWrite(m_p, speed);
      delay(on_duration);
      analogWrite(m_p, 0);
      delay(off_duration);
    }

    void stopMotor () {  // Stop motor
      digitalWrite(m_p, LOW);
      digitalWrite(s_p_1, LOW);
      digitalWrite(s_p_2, LOW);
    }

};